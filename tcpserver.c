
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include "constants.h"
#if defined(__APPLE__)
#  define COMMON_DIGEST_FOR_OPENSSL
#  include <CommonCrypto/CommonDigest.h>
#  define SHA1 CC_SHA1
#else
#  include <openssl/md5.h>
#endif

//file found = 1
//file not found = 0

//write and overwrite =1
//apend =0

//tcpserver port
//argc1, argv0: tcpclient.exe
//argc2, argv1: IP address TCP Server
int MD5_Init(MD5_CTX *c);
int MD5_Update(MD5_CTX *c, const void *data, size_t len);
int MD5_Final(unsigned char *md, MD5_CTX *c);

char *str2md5(const char *str, int length) {
    int n;
    MD5_CTX c;
    unsigned char digest[16];
    char *out = (char*)malloc(33);

    MD5_Init(&c);

    while (length > 0) {
        if (length > 512) {
            MD5_Update(&c, str, 512);
        } else {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n) {
        snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }

    return out;
}

int main( int argc, char *argv[] )
{
    int sockfd, newsockfd, serverPort, clientLength, nBytes;;
    char buffer[BUFSIZ];
    char option=' ';
    int sent=0;
    char s;
    struct sockaddr_in serverAddr, clientAddr;
    unsigned char *md;

 // printf("Ma hoa thu\n");
 //    md = str2md5("hello", strlen("hello"));
 //        printf("%s\n", md);
        

/* Check parameters from command line */

    if(argc != 2){//check number of argc in command line
        fprintf(stderr,"Usage: tcpserver [server_port]\n");
        return USAGE_ERR;
    }

    printf("Starting TCP server...\n");

    serverPort = atoi(argv[1]);
    if(serverPort<=0 || serverPort>65535)//check number of TCP server port
    {
        fprintf(stderr, "The port number given is wrong.\n");
        return BAD_PORT_NUM_ERR;
    }

    /* Create a TCP socket */

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)//check TCP socket is created correctly
    {
        perror("Error opening TCP socket");
        return SOCK_OPEN_ERR;
    }

    /* Initialize TCP socket structure */

    bzero((char *) &serverAddr, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(serverPort);

    /* Bind the host address using bind() call */

    if (bind(sockfd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)//check TCP socket is bind correctly
    {
        perror("Error on binding TCP socket");
        return SOCK_BIND_ERR;
    }

    /* Start listening for the clients, TCP server wait for the incoming TCP client connection */

    listen(sockfd, BACKLOG);
    //backlog max connections hang doi

    while(1) {

        clientLength = sizeof(clientAddr);

        /* Accept actual connection from the TCP client */

        newsockfd = accept(sockfd, (struct sockaddr *) &clientAddr, (socklen_t *) &clientLength);
        if (newsockfd < 0)//check actual connection is created correctly
        {
            perror("Error on accepting actual TCP client");
            return ACC_CONN_ERR;
        }

        /* Start communicating between TCP client and server */

        if(fork() == 0){
            close(sockfd);
            bzero(buffer,BUFSIZ);
            nBytes = read( newsockfd,buffer,BUFSIZ );
            //doc thong tin file gui len tu client
            if (nBytes < 0)
            {
                perror("Error reading from TCP socket");
                return SOCK_READ_ERR;
            }
            printf("This is the file to transffer: %s\n",buffer);

            /* Searching the file or creating file */
            FILE * file;
            char sample[10];
            if (fopen(buffer, "r") != NULL)
            {
                nBytes = write(newsockfd,"1",1);//neu da co file ton tai thi gui 1
                if (nBytes < 0)
                {
                    perror("Error writing to TCP socket");
                    return SOCK_WRITE_ERR;
                }
            }
            else
            {
                nBytes = write(newsockfd,"0",1);//neu khong co file ton tai thi gui 0
                if (nBytes < 0)
                {
                    perror("Error writing to TCP socket");
                    return SOCK_WRITE_ERR;
                }
            }


            nBytes = read( newsockfd, sample,1 );//doc respon ghi de file hay ko
            if (nBytes < 0)
            {
                perror("Error reading from TCP socket");
                return SOCK_READ_ERR;
            }

            option = sample[0];
            char readBuffer[NETWORK_BUFFER];

            
            while((nBytes = read(newsockfd, readBuffer, NETWORK_BUFFER)) > 0)
            {   
                
                file = fopen(buffer, "w+");
                if(nBytes < 0)
                {
                    fprintf(stderr, "Error while receiving the file.\n");
                    return FILE_RCV_ERR;
                }
                fprintf(file, readBuffer);
                md = str2md5(readBuffer, strlen(readBuffer));
                nBytes = write(newsockfd, md, strlen(md));
                free(md);
                // fprintf(stderr, readBuffer);
                if(readBuffer == EOF)
                {
                    fprintf(stderr, "Cannot write on file");
                    break;
                }
                if(readBuffer == "\0")
                {
                    fprintf(stderr, "Cannot receive.");
                    break;
                }
                fprintf(stderr, "\nReceive completed");
                // i
                fclose(file);
            }
            // fclose(file);
            sent=1;


            /* Write a response to the TCP client */
            if (1)
            {
                nBytes = write(newsockfd,"Transfer completed and successful",33);
                fprintf(stderr, "\nTransfer and create MD5 hash successful\n");
            }
            else
            {
                printf("Error writting the file in TCP server");
            }
            if (nBytes < 0)
            {
                perror("Error writing to TCP socket");
                return SOCK_WRITE_ERR;
            }
            

            
            close(newsockfd);
            exit(0);
        }
    }

    close(newsockfd);
    // close(sockfd);
    return 0;
}
