#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void sig_chld(int signo)
{
pid_t pid;
int stat;
while((pid = waitpid(-1, &stat, WNOHANG))>0)
printf("child %d terminated\n", pid);
return;
}
int main()
{
int listen_sock, conn_sock;
int server_len, client_len;
struct sockaddr_in server_address;
struct sockaddr_in client_address;

listen_sock = socket(AF_INET, SOCK_STREAM, 0);
if (listen_sock == -1)
 {
    printf("khong tao duoc socket\n");
    return 0;
 }
 printf("Tao socket thanh cong\n");

server_address.sin_family = AF_INET;
inet_aton("127.0.0.1",&server_address.sin_addr);
server_address.sin_port = htons(5500);
server_len = sizeof(server_address);

if(bind(listen_sock, (struct sockaddr *)&server_address,server_len)<0)
{
	printf("bind failed.\n");
    return 0;
}
printf("bind done\n");
int check = listen(listen_sock,10);
if (check == -1)
 {
 printf("error connect");
 return 0;
 }
printf("waiting connect ...\n");
while(1) {
client_len = sizeof(client_address);
conn_sock = accept(listen_sock,(struct sockaddr *)&client_address, &client_len);
if(conn_sock==-1){
	printf("error connect\n");
	return 1;
}else{
	printf("Accept new connection\n");
}
if(fork() == 0){
	close(listen_sock);

	int sentBytes,revedBytes,i;
	char buff[1024];
	    while((revedBytes = recv(conn_sock,buff,1024,0)) > 0){
	        buff[revedBytes]='\0';
	        /*if(strcmp(buff,"Q")==0||strcmp(buff,"q")==0) break;*/
	        printf("string send by client : %s\n",buff);
	        for (i = 0; i < strlen(buff);i++)
	        {
	            if(buff[i]>96&&buff[i]<123)
	                buff[i]=buff[i]-32;
	        }
	    	sentBytes=send(conn_sock,buff,1024,0);
	    }
	    
	close(conn_sock);
	exit(0);
}
signal(SIGCHLD,sig_chld);
close(conn_sock);
}
return 1;
}
