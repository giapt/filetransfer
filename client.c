#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#define MAX 81

char *inputString(int size){
  int test=0;
  char *s=(char*)malloc(size);
  do{
    if(test!=0){
      printf("Moi nhap lai : ");
    }
  fgets(s,size,stdin);
  test++;
  }while(strlen(s)<=1);
  return strtok(s,"\n");
}

int main(int argc, char *argv[]){
int sockfd;
struct sockaddr serverAddr;
char buff[1024];
struct sockaddr_in inAddr;

sockfd=socket(AF_INET,SOCK_STREAM,0);
if (sockfd == -1)
    {
        printf("khong tao duoc socket\n");
        return 1;
    }
printf("Tao socket thanh cong\n");
inAddr.sin_family=AF_INET;
inAddr.sin_port=htons(5500);
inet_aton("127.0.0.1",&inAddr.sin_addr);

if(connect(sockfd,(struct sockaddr *)&inAddr,sizeof(struct sockaddr))<0){
	printf("connect failed.\n");
    return 1;
}
  printf("Connection accepted\n");
  char *FileName;
  char *Result;
  printf("Nhap ten file gui : ");
  FileName = inputString(20);
  FILE *fpt = fopen(FileName,"r");
  if(fpt==NULL){
    printf("Khong tim thay file");
    return -1;
  }
  printf("Nhap ten file ket qua tra ve : ");
  Result = inputString(20);
  FILE *ft = fopen(Result,"w");
  
   long int sentBytes,revedBytes;
 // int Count=0;
  while(!feof(fpt)){
    if (fgets(buff,MAX,fpt) != NULL ){
    	sentBytes=send(sockfd,buff,1024,0);
   //	Count = Count + strlen(buff);
    	revedBytes=recv(sockfd,buff,1024,0);
   //   printf("%s\n",buff);
      fprintf(ft,"%s",buff);
    }
  }
  printf("Gui thanh cong\n");
  close(sockfd);
  fclose(fpt);fclose(ft);
//  printf("So byte da gui : %d\n",Count);
return 0;
}
